package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Signos;

public interface ISignosService {
	
	Signos registrar(Signos signo);
	
	void modificar(Signos signo);
	
	void eliminar(int idSignos);
	
	Signos listarId(int idSignos);
	
	List<Signos> listar();
	

}
