package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignosDAO;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@Service
public class SignosServiceImpl implements ISignosService {
	
	@Autowired
	private ISignosDAO dao;
	
	
	@Override
	public Signos registrar(Signos signo) {
		// TODO Auto-generated method stub
		Signos sign = new Signos();
		sign= dao.save(signo);
		
		return sign;
	}

	@Override
	public void modificar(Signos signo) {
		// TODO Auto-generated method stub
		dao.save(signo);
	}

	@Override
	public void eliminar(int idSignos) {
		// TODO Auto-generated method stub
		dao.delete(idSignos);
	}

	@Override
	public Signos listarId(int idSignos) {
		// TODO Auto-generated method stub
		return dao.findOne(idSignos);
	}

	@Override
	public List<Signos> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
